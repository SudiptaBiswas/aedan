package com.app.adensafe.view.activity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;

public abstract class BaseActivity extends AppCompatActivity {

    private boolean isAppInBackground = false;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    public void replaceFragment(Fragment mFragment, boolean addToBackStack) {
        FragmentManager manager = getSupportFragmentManager();
        Fragment fragment = manager.findFragmentByTag(mFragment.getClass().getSimpleName());
        if (fragment == null) {
            FragmentTransaction transaction = manager.beginTransaction();
            transaction.replace(getContainerId(), mFragment, mFragment.getClass().getSimpleName());
            if (addToBackStack)
                transaction.addToBackStack(mFragment.getClass().getSimpleName());
            if (!isAppInBackground)
                transaction.commit();
            else
                transaction.commitAllowingStateLoss();
        }
    }

    protected abstract int getContainerId();

    @Override
    protected void onPause() {
        super.onPause();
        isAppInBackground = true;
    }

    @Override
    protected void onResume() {
        super.onResume();
        isAppInBackground = false;
    }
}
