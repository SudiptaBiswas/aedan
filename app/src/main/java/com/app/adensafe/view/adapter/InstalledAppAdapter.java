package com.app.adensafe.view.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.app.adensafe.R;

public class InstalledAppAdapter extends RecyclerView.Adapter<InstalledAppAdapter.InstalledAppViewHolder> {
    @NonNull
    @Override
    public InstalledAppAdapter.InstalledAppViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.installed_app_item, parent, false);

        return new InstalledAppViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull InstalledAppAdapter.InstalledAppViewHolder holder, int position) {

    }

    @Override
    public int getItemCount() {
        return 10;
    }

    class InstalledAppViewHolder extends RecyclerView.ViewHolder {
        InstalledAppViewHolder(View itemView) {
            super(itemView);
        }
    }
}
