package com.app.adensafe.view.fragment;

import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.app.adensafe.R;
import com.app.adensafe.view.activity.BaseActivity;
import com.app.adensafe.view.adapter.CleanerAdapter;

import butterknife.BindView;
import butterknife.ButterKnife;

public class StartCleanerFragment extends Fragment {

    @BindView(R.id.cleaner_rv)
    RecyclerView cleaner_rv;
    @BindView(R.id.optimize_now_btn)
    TextView optimizationBtn;

    @BindView(R.id.circularProgressbar)
    ProgressBar circularProgressbar;

    int progressStatus = 0;
    private Handler handler = new Handler();

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_start_cleaner, container, false);
        ButterKnife.bind(this, view);
        initView();

        progress();

        return view;
    }


    private void initView() {
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        cleaner_rv.setLayoutManager(linearLayoutManager);
        cleaner_rv.setAdapter(new CleanerAdapter());

        optimizationBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((BaseActivity) getActivity()).replaceFragment(new CompleteCleanerFragment(), false);
            }
        });
    }

    private void progress()
    {
        // Start long running operation in a background thread
        new Thread(new Runnable() {
            public void run() {
                while (progressStatus < 100) {
                    progressStatus += 1;
                    // Update the progress bar and display the
                    //current value in the text view
                    handler.post(new Runnable() {
                        public void run() {
                            circularProgressbar.setProgress(progressStatus);
                            //textView.setText(progressStatus+"/"+progressBar.getMax());
                        }
                    });
                    try {
                        // Sleep for 200 milliseconds.
                        Thread.sleep(200);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        }).start();
    }
}
