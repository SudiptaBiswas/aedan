package com.app.adensafe.view.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.AppCompatCheckBox;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.app.adensafe.R;

public class CleanerAdapter extends RecyclerView.Adapter<CleanerAdapter.CleanerViewHolder> {

    public CleanerAdapter() {
    }

    @NonNull
    @Override
    public CleanerViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.cleaner_adapter_item, parent, false);

        return new CleanerViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull CleanerViewHolder holder, int position) {
        holder.item_desc_tv.setText("Ram Clean 45% Free");
    }

    @Override
    public int getItemCount() {
        return 5;
    }

    class CleanerViewHolder extends RecyclerView.ViewHolder {

        TextView item_desc_tv;
        AppCompatCheckBox cleaner_item_check_box;

        CleanerViewHolder(View itemView) {
            super(itemView);
            item_desc_tv = itemView.findViewById(R.id.item_desc_tv);
            cleaner_item_check_box = itemView.findViewById(R.id.cleaner_item_check_box);
        }
    }
}
