package com.app.adensafe.view.activity;

import android.os.Bundle;
import android.support.annotation.Nullable;

import com.app.adensafe.R;
import com.app.adensafe.view.fragment.StartCleanerFragment;

import butterknife.ButterKnife;

public class CleanerActivity extends BaseActivity {


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cleaner);
        ButterKnife.bind(this);

        replaceFragment(new StartCleanerFragment(), false);
    }

    @Override
    protected int getContainerId() {
        return R.id.cleaner_frame_container;
    }
}
