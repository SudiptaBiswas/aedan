package com.app.adensafe.view.activity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.app.adensafe.R;
import com.app.adensafe.view.adapter.InstalledAppAdapter;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SafeActivity extends AppCompatActivity {

    @BindView(R.id.installed_apps_rv)
    RecyclerView installed_apps_rv;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_safe);
        ButterKnife.bind(this);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        installed_apps_rv.setLayoutManager(linearLayoutManager);
        installed_apps_rv.setAdapter(new InstalledAppAdapter());
    }
}
